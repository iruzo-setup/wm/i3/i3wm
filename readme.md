# Personal i3wm config

- [Info](#info)
- [WARNING](#warning)
- [keybindings](#keybindings)

# Info

- What is i3wm ?
    - i3wm is a tiling window manager.
    - i3wm completely written from scratch.
    - [more info](https://i3wm.org/)

# WARNING

- In line 44 of [config](/../../blob/main/config) is set the default terminal. Change "st" for the one you prefer.

# keybindings

| Action      |                                                     | Key       |
|-------------|-----------------------------------------------------|-----------|
| lock screen | [i3lock](https://i3wm.org/i3lock/)                  | $mod + l  |
| browser     | [qutebrowser](https://qutebrowser.org/)             | $mod + o  |
